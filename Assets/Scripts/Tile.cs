﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[ExecuteInEditMode]
[RequireComponent(typeof(SpriteRenderer))]
public class Tile : MonoBehaviour
{
    public TileType Type;
    public TileMaterial Material;
    
    private SpriteRenderer _spriteRenderer;
    private string _spriteName;
    private float _positionX;
    private float _positionY;
    private float _gridSize = 0.64f;
    void OnEnable()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _spriteName = _spriteRenderer.sprite.name;
    }

    void Update()
    {
        AlignToGrid();
        SetSortOrder();
        SetTileMaterial();
    }

    private void AlignToGrid()
    {
        float reciprocalGrid = 1f / _gridSize;
        _positionX = Mathf.Round(transform.position.x * reciprocalGrid) / reciprocalGrid;
        _positionY = Mathf.Round(transform.position.y * reciprocalGrid) / reciprocalGrid;
        transform.position = new Vector3(_positionX, _positionY);
    }

    private void SetSortOrder()
    {
        int correction;
        switch (Type)
        {
            case TileType.Floor: correction = -4;
                break;
            case TileType.WallSE: correction = 1;
                break;
            case TileType.WallSW: correction = 1;
                break;
            case TileType.WallSCorner: correction = 1;
                break;
            case TileType.WallSFull: correction = 1;
                break;
            default: correction = 0;
                break;
        }
        float reciprocalGrid = 2f / _gridSize;
        _spriteRenderer.sortingOrder = - (int)Mathf.Round(_positionY * reciprocalGrid) + correction;
    }

    private void SetTileMaterial()
    {
        string path;
        switch (Material)
        {
            case TileMaterial.Wooden:
                path = "SpriteTiles/wooden/";
                break;
            case TileMaterial.Stone:
                path = "SpriteTiles/stone/";
                break;
            default:
                path = "SpriteTiles/base/";
                break;
        }
        _spriteRenderer.sprite = Resources.Load<Sprite>(string.Format("{0}{1}", path, _spriteName));
    }
}

public enum TileType
{
    Floor, WallECorner, WallEFull, WallNCornerl, WallNE, WallNFull, WallNW, WallSCorner, WallSE, WallSFull, WallSW, WallWCorner, WallWFull
}

public enum TileMaterial
{
    Base, Wooden, Stone
}
