﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TilesController))]
public class TilesControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TilesController tiles = (TilesController)target;
        if (GUILayout.Button("Change Materials"))
        {
            tiles.ChangeMaterials();
        }
        
    }
}
