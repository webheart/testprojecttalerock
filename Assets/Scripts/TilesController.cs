﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[ExecuteInEditMode]
public class TilesController : MonoBehaviour
{
    public TileMaterial GlobalTileMaterial;
    public void ChangeMaterials()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var tileObject = transform.GetChild(i).GetComponent<Tile>();
            if (tileObject == null) continue;
            tileObject.Material = GlobalTileMaterial;
        }
    }
    
}
